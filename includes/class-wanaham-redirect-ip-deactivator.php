<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/includes
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Redirect_Ip_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
