<?php

/**
 * Fired during plugin activation
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/includes
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Redirect_Ip_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
