<?php
/**
 * Created by PhpStorm.
 * User: Wanaham
 * Date: 02/07/2015
 * Time: 10:11
 */
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/maxmind/geoip.inc';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/maxmind/geoipcity.inc';

class Wanaham_Redirect_Ip_Geoip {

    /**
     * The Path of the dat file
     *
     * @since    1.0.0
     * @access   protected
     * @var      String    $GeoLiteCityPath
     */
    private $GeoLiteCityPath = '';

    private $langsSupported = array();

    private $plugin_name;

    public function __construct($plugin_name) {

        $this->plugin_name = $plugin_name;

        $this->GeoLiteCityPath = plugin_dir_path( dirname( __FILE__ ) ) .'includes/maxmind/GeoIP.dat';//LiteCity

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);

                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '') $langs[$lang] = 1;
                }

                // sort list based on value
                arsort($langs, SORT_NUMERIC);
                $this->langsSupported = $langs;
            }
        }

    }

    public function initGeoIp(){

        return geoip_open($this->GeoLiteCityPath,GEOIP_STANDARD);

    }

    public function closeGeoIp($gi){

        return geoip_close($gi);

    }

    public function getCountryByAddr($ip = false){

        $gi = $this->initGeoIp();

        $ip = $ip === false ? $_SERVER['REMOTE_ADDR'] : $ip;

        $country =  geoip_country_code_by_addr($gi,$ip);

        write_log($ip.':'.$country,$this->plugin_name);

        $this->closeGeoIp($gi);

        return $country;

    }

    public function hasLanguage($country_code){
        $locales = $this->country_code_to_locale($country_code);
        if(count($locales)){
            $locales_m =  array_map("strtolower", $locales);
            $langsSupported = array_keys($this->langsSupported);
            $result = array_intersect($locales_m, $langsSupported);
            write_log('locales ('.implode(',',$result).') from '.$country_code,$this->plugin_name);
            return count($result) > 0;
        }else{
            write_log('no locale from '.$country_code,$this->plugin_name);
        }
        return false;
    }

    /**
    /* Returns a locale from a country code that is provided.
    /*
    /* @param $country_code  ISO 3166-2-alpha 2 country code
    /* @param $language_code ISO 639-1-alpha 2 language code
    /* @returns  a locale, formatted like en_US, or null if not found
    /**/
    function country_code_to_locale($country_code, $language_code = '')
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = array(
        'af-ZA',
        'am-ET',
        'ar-AE',
        'ar-BH',
        'ar-DZ',
        'ar-EG',
        'ar-IQ',
        'ar-JO',
        'ar-KW',
        'ar-LB',
        'ar-LY',
        'ar-MA',
        'arn-CL',
        'ar-OM',
        'ar-QA',
        'ar-SA',
        'ar-SY',
        'ar-TN',
        'ar-YE',
        'as-IN',
        'az-Cyrl-AZ',
        'az-Latn-AZ',
        'ba-RU',
        'be-BY',
        'bg-BG',
        'bn-BD',
        'bn-IN',
        'bo-CN',
        'br-FR',
        'bs-Cyrl-BA',
        'bs-Latn-BA',
        'ca-ES',
        'co-FR',
        'cs-CZ',
        'cy-GB',
        'da-DK',
        'de-AT',
        'de-CH',
        'de-DE',
        'de-LI',
        'de-LU',
        'dsb-DE',
        'dv-MV',
        'el-GR',
        'en-029',
        'en-AU',
        'en-BZ',
        'en-CA',
        'en-GB',
        'en-IE',
        'en-IN',
        'en-JM',
        'en-MY',
        'en-NZ',
        'en-PH',
        'en-SG',
        'en-TT',
        'en-US',
        'en-ZA',
        'en-ZW',
        'es-AR',
        'es-BO',
        'es-CL',
        'es-CO',
        'es-CR',
        'es-DO',
        'es-EC',
        'es-ES',
        'es-GT',
        'es-HN',
        'es-MX',
        'es-NI',
        'es-PA',
        'es-PE',
        'es-PR',
        'es-PY',
        'es-SV',
        'es-US',
        'es-UY',
        'es-VE',
        'et-EE',
        'eu-ES',
        'fa-IR',
        'fi-FI',
        'fil-PH',
        'fo-FO',
        'fr-BE',
        'fr-CA',
        'fr-CH',
        'fr-FR',
        'fr-LU',
        'fr-MC',
        'fy-NL',
        'ga-IE',
        'gd-GB',
        'gl-ES',
        'gsw-FR',
        'gu-IN',
        'ha-Latn-NG',
        'he-IL',
        'hi-IN',
        'hr-BA',
        'hr-HR',
        'hsb-DE',
        'hu-HU',
        'hy-AM',
        'id-ID',
        'ig-NG',
        'ii-CN',
        'is-IS',
        'it-CH',
        'it-IT',
        'iu-Cans-CA',
        'iu-Latn-CA',
        'ja-JP',
        'ka-GE',
        'kk-KZ',
        'kl-GL',
        'km-KH',
        'kn-IN',
        'kok-IN',
        'ko-KR',
        'ky-KG',
        'lb-LU',
        'lo-LA',
        'lt-LT',
        'lv-LV',
        'mi-NZ',
        'mk-MK',
        'ml-IN',
        'mn-MN',
        'mn-Mong-CN',
        'moh-CA',
        'mr-IN',
        'ms-BN',
        'ms-MY',
        'mt-MT',
        'nb-NO',
        'ne-NP',
        'nl-BE',
        'nl-NL',
        'nn-NO',
        'nso-ZA',
        'oc-FR',
        'or-IN',
        'pa-IN',
        'pl-PL',
        'prs-AF',
        'ps-AF',
        'pt-BR',
        'pt-PT',
        'qut-GT',
        'quz-BO',
        'quz-EC',
        'quz-PE',
        'rm-CH',
        'ro-RO',
        'ru-RU',
        'rw-RW',
        'sah-RU',
        'sa-IN',
        'se-FI',
        'se-NO',
        'se-SE',
        'si-LK',
        'sk-SK',
        'sl-SI',
        'sma-NO',
        'sma-SE',
        'smj-NO',
        'smj-SE',
        'smn-FI',
        'sms-FI',
        'sq-AL',
        'sr-Cyrl-BA',
        'sr-Cyrl-CS',
        'sr-Cyrl-ME',
        'sr-Cyrl-RS',
        'sr-Latn-BA',
        'sr-Latn-CS',
        'sr-Latn-ME',
        'sr-Latn-RS',
        'sv-FI',
        'sv-SE',
        'sw-KE',
        'syr-SY',
        'ta-IN',
        'te-IN',
        'tg-Cyrl-TJ',
        'th-TH',
        'tk-TM',
        'tn-ZA',
        'tr-TR',
        'tt-RU',
        'tzm-Latn-DZ',
        'ug-CN',
        'uk-UA',
        'ur-PK',
        'uz-Cyrl-UZ',
        'uz-Latn-UZ',
        'vi-VN',
        'wo-SN',
        'xh-ZA',
        'yo-NG',
        'zh-CN',
        'zh-HK',
        'zh-MO',
        'zh-SG',
        'zh-TW',
        'zu-ZA',);
        $return = [];
        foreach ($locales as $locale)
        {

            if(substr($locale,-strlen($country_code)) === $country_code){
                $return[] = $locale;
            }
/*          //intl must be setup first

            $locale_region = locale_get_region($locale);
            $locale_language = locale_get_primary_language($locale);
            $locale_array = array('language' => $locale_language,
                'region' => $locale_region);

            if (strtoupper($country_code) == $locale_region &&
                $language_code == '')
            {
                return locale_compose($locale_array);
            }
            elseif (strtoupper($country_code) == $locale_region &&
                strtolower($language_code) == $locale_language)
            {
                return locale_compose($locale_array);
            }*/
        }

        return count($return) ? $return : null ;
    }
} 