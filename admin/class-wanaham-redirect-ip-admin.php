<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/admin
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Redirect_Ip_Admin {

    protected $option_group_name = '';

    protected $option_name = '';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        $plugin_name_alt = str_replace('-','_',$plugin_name);

        $this->option_group_name = $plugin_name_alt.'_option_group';

        $this->option_name = $plugin_name_alt.'_option';

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Redirect_Ip_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Redirect_Ip_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wanaham-redirect-ip-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Redirect_Ip_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Redirect_Ip_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wanaham-redirect-ip-admin.js', array( 'jquery' ), $this->version, false );

	}

    public function add_plugin_page(){
        add_options_page(
            __('Redirect from IP & Lang',$this->plugin_name),
            __('Redirect from IP & Lang',$this->plugin_name),
            'manage_options',
            $this->plugin_name,
            array( $this, 'create_admin_page' )
        );
    }

    public function create_admin_page(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/wanaham-redirect-ip-admin-display.php';
    }

    public function get_option( ){
        return get_option( $this->option_name );
    }

    public function page_init(){
        $this->options = $this->get_option();

        $geoip_countries = isset($this->options['geoip_countries']) ? $this->options['geoip_countries'] : [];

        register_setting(
            $this->option_group_name, // Option group
            $this->option_name, // Option name
            array( $this, 'validate' ) // Sanitize
        );

        add_settings_section(
            'geoip_countries_choice', // ID
            'Les pays', // Title
            null, // Callback
            $this->plugin_name // Page
        );

        add_settings_field(
            'geoip_countries', // ID
            __("Sélectionnez un ou plusieurs pays",$this->plugin_name),
            array( $this, 'getCountryCheckboxes' ), // Callback
            $this->plugin_name, // Page
            'geoip_countries_choice' // Section
        );

        if(count($geoip_countries) > 0){

            $geoip = new Wanaham_Redirect_Ip_Geoip($this->plugin_name);
            $gi = $geoip->initGeoIp();

            add_settings_section(
                'geoip_countries_redirect_url',
                __("Les redirections",$this->plugin_name),
                null,
                $this->plugin_name
            );

            foreach($geoip_countries as $geoip_country){
                add_settings_field(
                    'geoip_country_redirect_url_'.$geoip_country, // ID
                    __("Renseignez une addresse de redirection pour ",$this->plugin_name).': '.$gi->GEOIP_COUNTRY_NAMES[$gi->GEOIP_COUNTRY_CODE_TO_NUMBER[$geoip_country]],
                    array( $this, 'getCountryRedirectUrl' ), // Callback
                    $this->plugin_name, // Page
                    'geoip_countries_redirect_url', // Section
                    $geoip_country
                );
            }
            $geoip->closeGeoIp($gi);
        }

        /*
        add_settings_section(
            'setting_section_id', // ID
            'My Custom Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            $this->plugin_name // Page
        );

        add_settings_field(
            'id_number', // ID
            'ID Number', // Title
            array( $this, 'id_number_callback' ), // Callback
            $this->plugin_name, // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'title',
            'Title',
            array( $this, 'title_callback' ),
            $this->plugin_name,
            'setting_section_id'
        );
        */
    }

    public function validate( $input )
    {

        $new_input = array();
        if( isset( $input['geoip_countries'] ) && is_array($input['geoip_countries'])){
            $new_input['geoip_countries'] = $input['geoip_countries'];
            if(count($new_input['geoip_countries'])> 0){
                $new_input['geoip_redirect_urls'] = [];
                foreach($new_input['geoip_countries'] as $country_code){
                    if(isset($input['geoip_country_redirect_url_'.$country_code])){
                        if(filter_var($input['geoip_country_redirect_url_'.$country_code], FILTER_VALIDATE_URL)){
                            $new_input['geoip_redirect_urls'][$country_code] = filter_var($input['geoip_country_redirect_url_'.$country_code], FILTER_SANITIZE_URL);
                        }else{
                            add_settings_error( $this->option_name, 'invalid-url', sprintf(__("Invalid URL for %s",$this->plugin_name),$country_code) );
                        }
                    }
                }
            }
        }


        return $new_input;

    }

    public function getCountryCheckboxes(){
        $geoip = new Wanaham_Redirect_Ip_Geoip($this->plugin_name);
        $gi = $geoip->initGeoIp();
        $html = '';
        $geoip_countries = isset($this->options['geoip_countries']) ? $this->options['geoip_countries'] : [];
        foreach($gi->GEOIP_COUNTRY_CODES as $key=>$country_code){
            if(empty($country_code)){
                continue;
            }
            $id = $key.'_'.$country_code;
            $selected = in_array($country_code,$geoip_countries) ? ' checked="checked" ': '';
            $html .= sprintf('<label class="wanaham_geoip_country" for="%s"><input %s id="%s" type="checkbox" name="%s[geoip_countries][]" value="%s" />%s (%s)</label>',$id,$selected, $id,$this->option_name,$country_code, $gi->GEOIP_COUNTRY_NAMES[$key],$country_code);
        }
        echo '<div id="wanaham_geoip_country_container">',$html,'</div>';
        $geoip->closeGeoIp($gi);
    }

    public function getCountryRedirectUrl($geoip_country){
        $geoip_url = isset($this->options['geoip_redirect_urls']) && isset($this->options['geoip_redirect_urls'][$geoip_country]) ? $this->options['geoip_redirect_urls'][$geoip_country] : '';
        echo sprintf('<input type="text" name="%s[geoip_country_redirect_url_%s]" size="40" placeholder="http://your.website.%s" value="%s" />',$this->option_name,$geoip_country,strtolower($geoip_country),$geoip_url);
    }

}
