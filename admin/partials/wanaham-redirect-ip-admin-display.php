<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/admin/partials
 */


?>
<div class="wrap">
    <h2><?php echo __("Wanaham redirect ip & lang",$this->plugin_name); ?></h2>
    <form method="post" action="options.php">
        <?php
        // This prints out all hidden setting fields
        settings_fields( $this->option_group_name );
        do_settings_sections( $this->plugin_name );
        submit_button();
        ?>
    </form>
    <small>This product includes GeoLite data created by MaxMind, available from <a href="http://www.maxmind.com">http://www.maxmind.com</a>.</small>
</div>
