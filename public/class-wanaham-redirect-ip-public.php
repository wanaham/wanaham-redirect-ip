<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wanaham_Redirect_Ip
 * @subpackage Wanaham_Redirect_Ip/public
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Redirect_Ip_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */

    private $options = array();

    private $deactivated = false;

    private $site_ur = '';

	public function __construct( $plugin_name, $version,$options ) {

		$this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->options = $options;
        $this->site_url = get_site_url();

        if(isset($_SERVER['HTTP_REFERER'])){
            $pos = strrpos($_SERVER['HTTP_REFERER'],$this->site_url);
            if ($pos !== false) {
                $this->deactivated = true;
            }
        }

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Redirect_Ip_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Redirect_Ip_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wanaham-redirect-ip-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Redirect_Ip_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Redirect_Ip_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wanaham-redirect-ip-public.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Redirect user from his ip and navigator language
     */
    public function redirect(){
        if($this->deactivated){
            return false;
        }
        $geoip = new Wanaham_Redirect_Ip_Geoip($this->plugin_name);

        $ip = false;
        //$ip = '192.206.151.131';//Toronto
        //$ip = '3.255.255.255'; // US
        //$ip = '2.223.255.255'; // GB
        //$ip = '89.91.148.159';//FR
        //$ip = '24.24.24.24';//MaxMind

        $country = $geoip->getCountryByAddr($ip);

        if($country === null){
            return false;
        }

        $geoip_redirect_urls = isset($this->options['geoip_redirect_urls']) ? $this->options['geoip_redirect_urls'] : array();

        if(isset($geoip_redirect_urls[$country]) && $this->site_url !== $geoip_redirect_urls[$country] && $geoip->hasLanguage($country)){
            wp_redirect( $geoip_redirect_urls[$country] );
            exit;
        }
    }

}
