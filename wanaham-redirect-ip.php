<?php

/**
 * @TODO
 * plugin descripion
 *
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.wanaham.com
 * @since             1.0.0
 * @package           Wanaham_Redirect_Ip
 *
 * @wordpress-plugin
 * Plugin Name:       Wanaham redirect ip & lang
 * Plugin URI:        https://gitlab.com/wanaham/wanaham-redirect-ip
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Wanaham
 * Author URI:        www.wanaham.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wanaham-redirect-ip
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if (!function_exists('write_log')) {
    function write_log ( $log, $fileName )  {
        if ( true === WP_DEBUG ) {
            $upload_dir = wp_upload_dir();
            $file = $upload_dir['basedir'].'/'.$fileName.'.log';
            $prefix = "\n".date(DATE_RFC2822)."\t";
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( $prefix.print_r( $log, true )."\n" , 3, $file);
            } else {
                error_log( $prefix.$log , 3, $file);
            }
        }
    }
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wanaham-redirect-ip-activator.php
 */
function activate_wanaham_redirect_ip() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wanaham-redirect-ip-activator.php';
	Wanaham_Redirect_Ip_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wanaham-redirect-ip-deactivator.php
 */
function deactivate_wanaham_redirect_ip() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wanaham-redirect-ip-deactivator.php';
	Wanaham_Redirect_Ip_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wanaham_redirect_ip' );
register_deactivation_hook( __FILE__, 'deactivate_wanaham_redirect_ip' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wanaham-redirect-ip.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wanaham_redirect_ip() {

	$plugin = new Wanaham_Redirect_Ip();
	$plugin->run();

}
run_wanaham_redirect_ip();
